function validateCheckBoxDays(){
	var checkBoxDaysNodeList = document.getElementById("bestDaysToContactDiv").getElementsByTagName("input");
	var bABoxChecked = false;

	for ( var checkBoxDay of checkBoxDaysNodeList ) {
		if ( checkBoxDay.checked )
			bABoxChecked = true;
	}

	return bABoxChecked;
}

function validateName(){
	var nameValue = document.getElementById("name").value.trim();
	var bValidName = false;
	
	if ( nameValue.length > 0 && nameValue.search(new RegExp("[A-z]")) != -1 ) bValidName = true;

	return bValidName;
}

function validateEmail(){
	var emailValue = document.getElementById("email").value.trim();
	var bValidEmail = false;
	
	if ( emailValue.length > 5 && emailValue.search(new RegExp("[A-z]")) != -1 && emailValue.search("@") != -1 ) bValidEmail = true;

	return bValidEmail;
}

function validatePhone(){
	var phoneValue = document.getElementById("phone").value.trim();
	var bValidPhone = false;
	
	if ( phoneValue.length > 6 && phoneValue.search(new RegExp("[^A-z]")) != -1 ) bValidPhone = true;

	return bValidPhone;
}

function validateContactInfo() {
	var bValidContactInfo = false;

	if ( validateName() && ( validateEmail() || validatePhone() ) ) bValidContactInfo = true;

	return bValidContactInfo;
}

function validateReasonForInquiry(){
	var bValidReasonForInquiry = true;
	
	if ( document.getElementById("reasonForInquiry").value == "other" ) {
		bValidReasonForInquiry = validateAdditionalInfo();
	}

	return bValidReasonForInquiry;
}

function validateAdditionalInfo(){
	var bValidAdditionalInfo = false;
	var additionalInfoValue = document.getElementById("additionalInformation").value.trim();

	if ( additionalInfoValue.length > 5 && additonalInfoValue.search(new RegExp("[A-z]")) ) bValidAdditionalInfo = true;

	return bValidAdditionalInfo;
}

function validateForm(){

	var bValid = false;

	if ( !validateContactInfo() ) {
		alert("Please include your name and either a phone number or Email address.");

	} else if ( !validateReasonForInquiry() ) {
		alert("Please include a description of your reason for inquring in the 'Additional Information' box.");

	} else if ( !validateCheckBoxDays() ) {
		alert("Please Select At Least One Day That We May Contact You On.");

	} else {
		console.log("Contact Form Succesfully Validated.");
		bValid = true;
	}
	
	return bValid;	
}